﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalcBase2.Test
{
    [TestClass]
    public class CalculatorBase2Test
    {
        [TestMethod]
        public void Suma1Mas1()
        {
            var calc = new CalculatorBase2();
            Assert.AreEqual("10", calc.Suma("1", "1"));
        }

        [TestMethod]
        public void Suma0010Mas0110()
        {
            var calc = new CalculatorBase2();
            Assert.AreEqual("1000", calc.Suma("0010", "0110"));
        }

        [TestMethod]
        public void Resta1Menos1()
        {
            var calc = new CalculatorBase2();
            Assert.AreEqual("0", calc.Resta("1", "1"));
        }

        [TestMethod]
        public void Multiplicacion1Por1()
        {
            var calc = new CalculatorBase2();
            Assert.AreEqual("1", calc.Multiplicacion("1", "1"));
        }

        [TestMethod]
        public void Division1Sobre1()
        {
            var calc = new CalculatorBase2();
            Assert.AreEqual("1", calc.Division("1", "1"));
        }
    }
}
