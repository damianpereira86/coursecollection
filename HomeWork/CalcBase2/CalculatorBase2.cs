﻿using System;

namespace CalcBase2
{
    public class CalculatorBase2 : ICalculator
    {
        public string Suma(string a, string b)
        {
            int intA = Convert.ToInt32(a, 2);
            int intB = Convert.ToInt32(b, 2);
            int result = intA + intB;
            return Convert.ToString(result, 2);
        }

        public string Resta(string a, string b)
        {
            int intA = Convert.ToInt32(a, 2);
            int intB = Convert.ToInt32(b, 2);
            int result = intA - intB;
            return Convert.ToString(result, 2);
        }

        public string Multiplicacion(string a, string b)
        {
            int intA = Convert.ToInt32(a, 2);
            int intB = Convert.ToInt32(b, 2);
            int result = intA * intB;
            return Convert.ToString(result, 2);
        }

        public string Division(string a, string b)
        {
            int intA = Convert.ToInt32(a, 2);
            int intB = Convert.ToInt32(b, 2);
            int result = intA / intB;
            return Convert.ToString(result, 2);
        }
    }
}
