﻿namespace CalcBase2
{
    public interface ICalculator
    {
        string Suma(string a, string b);
        string Resta(string a, string b);
        string Division(string a, string b);
        string Multiplicacion(string a, string b);
    }
}
