﻿namespace CollectionOperations.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using CollectionOperations;
    using System.Collections.Generic;
    using System.Linq;

    [TestClass]
    public class OperationsTest
    {
        private IBinaryCollectionOperations Operations;

        [TestInitialize]
        public void Setup()
        {
            this.Operations = new Operations();
        }

        [TestMethod]
        public void OrderAscTest()
        {
            // Test Collection 4, 2, 8
            var a = new[] { "100", "10", "1000" };
            var b = Operations.OrderAsc(a).ToArray();
            Assert.AreEqual("10", b[0]);
            Assert.AreEqual("100", b[1]);
            Assert.AreEqual("1000", b[2]);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void OrderAscCollectionNullTest()
        {
            IEnumerable<string> a = null;
            var b = Operations.OrderAsc(a);
        }

        [TestMethod]
        public void OrderDscTest()
        {
            var a = new[] { "100", "10", "1000" };
            var b = Operations.OrderDsc(a).ToArray();
            Assert.AreEqual("1000", b[0]);
            Assert.AreEqual("100", b[1]);
            Assert.AreEqual("10", b[2]);
        }

        [TestMethod]
        public void DivisibleBetween2Test()
        {
            // Test Collection 4, 2, 8, 6, 9, 15, 5
            var a = new[] { "100", "10", "1000", "110", "1001", "1111", "101" };
            var b = Operations.AreDivisible(a, 2).ToArray();
            Assert.IsTrue(b.Count() == 4);
            Assert.AreEqual("100", b[0]);
            Assert.AreEqual("10", b[1]);
            Assert.AreEqual("1000", b[2]);
            Assert.AreEqual("110", b[3]);
        }

        [TestMethod]
        public void DivisibleBetween3Test()
        {
            // Test Collection 4, 2, 8, 6, 9, 15, 5
            var a = new[] { "100", "10", "1000", "110", "1001", "1111", "101" };
            var b = Operations.AreDivisible(a, 3).ToArray();
            Assert.IsTrue(b.Count() == 3);
            Assert.AreEqual("110", b[0]);
            Assert.AreEqual("1001", b[1]);
            Assert.AreEqual("1111", b[2]);
        }

        [TestMethod]
        public void DivisibleBetween5Test()
        {
            // Test Collection 4, 2, 8, 6, 9, 15, 5
            var a = new [] { "100", "10", "1000", "110", "1001", "1111", "101" };
            var b = Operations.AreDivisible(a, 5).ToArray();
            Assert.IsTrue(b.Count() == 2);
            Assert.AreEqual("1111", b[0]);
            Assert.AreEqual("101", b[1]);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void DivisibleBetween1Test()
        {
            var a = new[] { "100", "10", "1000", "110", "1001", "1111", "101" };
            var b = Operations.AreDivisible(a, 1).ToArray();
            Assert.IsTrue(b.Count() == 2);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void DivisibleBetween20Test()
        {
            var a = new[] { "100", "10", "1000", "110", "1001", "1111", "101" };
            var b = Operations.AreDivisible(a, 20).ToArray();
            Assert.IsTrue(b.Count() == 2);
        }

        [TestMethod]
        public void Sum428Test()
        {
            // Test Collection 4, 2, 8
            var a = new[] { "100", "10", "1000"};
            var b = Operations.SumBinaryCollection(a);
            Assert.AreEqual("1110", b);
        }

        [TestMethod]
        public void Sum42869Test()
        {
            // Test Collection 4, 2, 8, 6, 9
            var a = new[] { "100", "10", "1000", "110", "1001" };
            var b = Operations.SumBinaryCollection(a);
            Assert.AreEqual("11101", b);
        }

        [TestMethod]
        public void Remove4Test()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100", "10", "100", "110", "1001" };
            var b = Operations.RemoveDuplicates(a).ToArray();
            Assert.AreEqual("100", b[0]);
            Assert.AreEqual("10", b[1]);
            Assert.AreEqual("110", b[2]);
            Assert.AreEqual("1001", b[3]);
        }

        [TestMethod]
        public void RemoveBoth4Test()
        {
            var a = new[] { "100", "10", "100", "1000", "100" };
            var b = Operations.RemoveDuplicates(a).ToArray();
            Assert.AreEqual("100", b[0]);
            Assert.AreEqual("10", b[1]);
            Assert.AreEqual("1000", b[2]);
        }

        [TestMethod]
        public void RemoveNothingTest()
        {
            var a = new[] { "100", "10", "1000"};
            var b = Operations.RemoveDuplicates(a).ToArray();
            Assert.AreEqual("100", b[0]);
            Assert.AreEqual("10", b[1]);
            Assert.AreEqual("1000", b[2]);
        }

        [TestMethod]
        public void Count0Test()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100", "10", "100", "110", "1001" };
            var b = Operations.CountDigit(a, '0');
            Assert.AreEqual(8, b);
        }

        [TestMethod]
        public void Count1Test()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100", "10", "100", "110", "1001" };
            var b = Operations.CountDigit(a, '1');
            Assert.AreEqual(7, b);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void Count2ExceptionTest()
        {
            var a = new[] { "100", "10", "100", "110", "1001" };
            var b = Operations.CountDigit(a, '2');
            Assert.AreEqual(0, b);
        }

        [TestMethod]
        public void CountEquals0Test()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "1", "11", "111"};
            var b = Operations.CountDigit(a, '0');
            Assert.AreEqual(0, b);
        }

        [TestMethod]
        public void ReverseCollectionTest()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100", "100", "10", "1000", "100" };
            var b = Operations.ReverseCollection(a).ToArray();
            Assert.AreEqual("100", b[0]);
            Assert.AreEqual("1000", b[1]);
            Assert.AreEqual("10", b[2]);
            Assert.AreEqual("100", b[3]);
            Assert.AreEqual("100", b[4]);
        }

        [TestMethod]
        public void Reverse1ItemCollectionTest()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100"};
            var b = Operations.ReverseCollection(a).ToArray();
            Assert.AreEqual("100", b[0]);
        }

        [TestMethod]
        public void ContainsBinaryTest()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100", "100", "10", "1000", "100" };
            var b = Operations.CollectionContainsBinary(a, "10");
            Assert.IsTrue(b);
        }

        [TestMethod]
        public void NotContainsBinaryTest()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100", "100", "10", "1000", "100" };
            var b = Operations.CollectionContainsBinary(a, "11");
            Assert.IsFalse(b);
        }

        [TestMethod]
        public void EmptyCollectionNotContainsBinaryTest()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new List<string>();
            var b = Operations.CollectionContainsBinary(a, "10");
            Assert.IsFalse(b);
        }

        [TestMethod]
        public void CountSeveralBinaryOccurrencesTest()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100", "100", "10", "1000", "100" };
            var b = Operations.CountBinaryOccurrences(a, "100");
            Assert.AreEqual(3, b);
        }

        [TestMethod]
        public void Count1BinaryOccurrenceTest()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100", "100", "10", "1000", "100" };
            var b = Operations.CountBinaryOccurrences(a, "10");
            Assert.AreEqual(1, b);
        }

        [TestMethod]
        public void Count0BinaryOccurrenceTest()
        {
            // Test Collection 4, 2, 4, 6, 9
            var a = new[] { "100", "100", "10", "1000", "100" };
            var b = Operations.CountBinaryOccurrences(a, "0");
            Assert.AreEqual(0, b);
        }
    }
}
