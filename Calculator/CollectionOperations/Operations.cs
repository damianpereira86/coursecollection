﻿namespace CollectionOperations
{
    using CalculatorMultiBase;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Operations : IBinaryCollectionOperations
    {
        private static IEnumerable<int> ConvertBinaryCollectionToInt(IEnumerable<string> binaryCollection)
        {
            return binaryCollection.Select(binary => Convert.ToInt32(BaseChange.ArbitraryToDecimalSystem(binary, 2))).ToList();
        }

        private static IEnumerable<string> ConvertIntCollectionToBinary(IEnumerable<int> integerCollection)
        {
            return integerCollection.Select(number => BaseChange.DecimalToArbitrarySystem(number, 2)).ToList();
        }

        ///<inheritdoc />
        public IEnumerable<string> OrderAsc(IEnumerable<string> binaryCollection)
        {
            if (binaryCollection == null) throw new ArgumentException("The collection is null.");
            var integerCollection = ConvertBinaryCollectionToInt(binaryCollection);
            integerCollection = integerCollection.OrderBy(i => i).ToList();
            var orderedBinaryList = ConvertIntCollectionToBinary(integerCollection);
            return orderedBinaryList;
        }

        ///<inheritdoc />
        public IEnumerable<string> OrderDsc(IEnumerable<string> binaryCollection)
        {
            if (binaryCollection == null) throw new ArgumentException("The collection is null.");
            var integerCollection = ConvertBinaryCollectionToInt(binaryCollection);
            integerCollection = integerCollection.OrderByDescending(i => i).ToList();
            var orderedBinaryList = ConvertIntCollectionToBinary(integerCollection);
            return orderedBinaryList;
        }

        ///<inheritdoc />
        public IEnumerable<string> AreDivisible(IEnumerable<string> binaryCollection, int divisor)
        {
            if (binaryCollection == null) throw new ArgumentException("The collection is null.");
            if ((divisor < 2) || (divisor > 19)) throw new ArgumentException("The divisor must be between 2 and 19.");
            var integerCollection = ConvertBinaryCollectionToInt(binaryCollection);
            return (from number in integerCollection where number % divisor == 0 select BaseChange.DecimalToArbitrarySystem(number, 2)).ToList();
        }

        ///<inheritdoc />
        public string SumBinaryCollection(IEnumerable<string> binaryCollection)
        {
            if (binaryCollection == null) throw new ArgumentException("The collection is null.");
            var calculator = new CalculatorBase2();
            var sum = binaryCollection.Aggregate("0", (current, binary) => calculator.Addition(current, binary));
            return sum;
        }

        ///<inheritdoc />
        public IEnumerable<string> RemoveDuplicates(IEnumerable<string> binaryCollection)
        {
            if (binaryCollection == null) throw new ArgumentException("The collection is null.");
            var newBinaryCollection = new List<string>();
            foreach (var binary in binaryCollection.Where(binary => !newBinaryCollection.Contains(binary)))
            {
                newBinaryCollection.Add(binary);
            }

            return newBinaryCollection;
        }

        ///<inheritdoc />
        public int CountDigit(IEnumerable<string> binaryCollection, char digit)
        {
            if (binaryCollection == null) throw new ArgumentException("The collection is null.");
            if ((digit.Equals('0')) || (digit.Equals('1')))
            {
                return binaryCollection.Sum(binary => binary.Count(x => x.Equals(digit)));
            }
            throw new ArgumentException("The digit should be 0 or 1.");
        }

        ///<inheritdoc />
        public IEnumerable<string> ReverseCollection(IEnumerable<string> binaryCollection)
        {
            if (binaryCollection == null) throw new ArgumentException("The collection is null.");
            return binaryCollection.Reverse();
        }

        ///<inheritdoc />
        public bool CollectionContainsBinary(IEnumerable<string> binaryCollection, string binary)
        {
            if (binaryCollection == null) throw new ArgumentException("The collection is null.");
            return binaryCollection.Contains(binary);
        }

        ///<inheritdoc />
        public int CountBinaryOccurrences(IEnumerable<string> binaryCollection, string binary)
        {
            if (binaryCollection == null) throw new ArgumentException("The collection is null.");
            return binaryCollection.Count(x => x.Equals(binary));
        }
    }
}
