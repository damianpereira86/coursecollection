﻿namespace CollectionOperations
{
    using System.Collections.Generic;

    public interface IBinaryCollectionOperations
    {
        /// <summary>
        ///     Orders a binary collection ascending.
        /// </summary>
        /// <param name="binaryCollection">
        ///     The binary collection to order.
        /// </param>
        /// <returns>
        ///     The specified binary collection ordered ascending.
        /// </returns>
        /// /// <exception>
        ///     T:System.ArgumentException: binaryCollection is null.
        /// </exception>
        IEnumerable<string> OrderAsc(IEnumerable<string> binaryCollection);

        /// <summary>
        ///     Orders a binary collection descending.
        /// </summary>
        /// <param name="binaryCollection">
        ///     The binary collection to order.
        /// </param>
        /// <returns>
        ///     The specified binary collection ordered descending.
        /// </returns>
        /// <exception>
        ///     T:System.ArgumentException: binaryCollection is null.
        /// </exception>
        IEnumerable<string> OrderDsc(IEnumerable<string> binaryCollection);

        /// <summary>
        ///     Returns all the binary numbers from a given collection that are divisible by a given number
        /// </summary>
        /// <param name="binaryCollection"></param>
        /// <param name="divisor"></param>
        /// <returns>
        ///     A collection of binary numbers that are divisible by the specified divisor
        /// </returns>
        /// <exception>
        ///     T:System.ArgumentException: binaryCollection is null.
        /// </exception>
        /// <exception>
        ///     T:System.ArgumentException: divisor is smaller than 2 or greater than 19.
        /// </exception>
        IEnumerable<string> AreDivisible(IEnumerable<string> binaryCollection, int divisor);

        /// <summary>
        ///     Sums all the binary numbers of a collection.
        /// </summary>
        /// <param name="binaryCollection">
        ///     The binary collection to sum its elements.
        /// </param>
        /// <returns>
        ///     The sum of all the numbers of the specified binary collection
        /// </returns>
        /// <exception>
        ///     T:System.ArgumentException: binaryCollection is null.
        /// </exception>
        string SumBinaryCollection(IEnumerable<string> binaryCollection);

        /// <summary>
        ///     Removes the duplicates from a binary collection.
        /// </summary>
        /// <param name="binaryCollection">
        ///     The binary collection to remove its duplicate elements.
        /// </param>
        /// <returns>
        ///     A binary collection without duplicates
        /// </returns>
        /// <exception>
        ///     T:System.ArgumentException: binaryCollection is null.
        /// </exception>
        IEnumerable<string> RemoveDuplicates(IEnumerable<string> binaryCollection);

        /// <summary>
        ///     Counts the number of digits (0s or 1s) of a binary collection.
        /// </summary>
        /// <param name="binaryCollection">
        ///     The binary collection to count its digits.
        /// </param>
        /// <param name="digit">
        ///     The digit to count. Must be 0 or 1.
        /// </param>
        /// <returns>
        ///     The number of specified digits of the binary collection
        /// </returns>
        /// <exception>
        ///     T:System.ArgumentException: binaryCollection or digit is null.
        /// </exception>
        int CountDigit(IEnumerable<string> binaryCollection, char digit);

        /// <summary>
        ///     Inverts the order of the elements in a binary collection.
        /// </summary>
        /// <param name="binaryCollection">
        ///     The binary collection to reverse.
        /// </param>
        /// <returns>
        ///     A binary collection whose elements correspond to those of the input binry collection in reverse order
        /// </returns>
        /// <exception>
        ///     T:System.ArgumentException: binaryCollection is null.
        /// </exception>
        IEnumerable<string> ReverseCollection(IEnumerable<string> binaryCollection);

        /// <summary>
        ///     Determines whether a binary collection contains a specified binary number
        /// </summary>
        /// <param name="binaryCollection">
        ///     A binnary collection in which to locate a a binary number.
        /// </param>
        /// <param name="binary">
        ///     The binary number to locate in the collection.
        /// </param>
        /// <returns>
        ///     True if the source binary collection contains binary number that has the specified value; otherwise, false.
        /// </returns>
        /// <exception>
        ///     T:System.ArgumentException: binaryCollection is null.
        /// </exception>
        bool CollectionContainsBinary(IEnumerable<string> binaryCollection, string binary);

        /// <summary>
        ///     Returns the amount of times that a specified binary number is in a binary collection
        /// </summary>
        /// <param name="binaryCollection"></param>
        /// <param name="binary">
        ///     The binary number that will be counted.
        /// </param>
        /// <returns>
        ///     The amount of times that the specified binary number is in the binary collection
        /// </returns>
        /// <exception>
        ///     T:System.ArgumentException: binaryCollection is null.
        /// </exception>
        int CountBinaryOccurrences(IEnumerable<string> binaryCollection, string binary);
    }
}