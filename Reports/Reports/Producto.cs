﻿namespace Reports
{
    public class Producto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public double ValorActual { get; set; }
        public string Descripcion { get; set; }
    }
}
