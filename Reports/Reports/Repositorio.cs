﻿namespace Reports
{
    using System.Collections.Generic;

    public static class Repositorio
    {
        public static IEnumerable<Factura> Facturas { get; set; }
        public static IEnumerable<Cliente> Clientes { get; set; }
        public static IEnumerable<Producto> Productos { get; set; }
    }
}
