﻿using System.ComponentModel;

namespace Reports.Utils.Enums
{
    public enum Moneda
    {
        [Description("Moneda desconocida")]
        Desconocida = 0,
        [Description("Dolares Americanos")]
        Dolar = 1,
        [Description("Euros")]
        Euro = 2
    }
}