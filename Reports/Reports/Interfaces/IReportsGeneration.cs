﻿namespace Reports.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;
    using Utils.Enums;

    public interface IReportsGeneration
    {
        /// <summary>
        ///     Retorna una coleccion con las facturas ordenadas por anio
        /// </summary>
        /// <returns> 
        ///     Una coleccion de Facturas ordenadas por anio
        /// </returns>
        IEnumerable<Factura> FacturasPorAnio();

        /// <summary>
        ///     Retorna una coleccion con los clientes que tienen facturas en un anio dado
        /// </summary>
        /// <param name="anio">
        ///     El anio en el que buscar las facturas
        /// </param>
        /// <returns>
        ///     Una coleccion con los clientes que tienen facturas en el anio dado
        /// </returns>
        IEnumerable<Cliente> ClientesConFacturasPorAnio(int anio);

        /// <summary>
        ///     Calcula los totales facturados por moneda por anio
        /// </summary>
        /// <returns>
        ///     Los totales facturados agrupdaos por moneda y por anio
        /// </returns>
        IEnumerable<IGrouping<Moneda, double>> TotalesFacturados();

        /// <summary>
        ///     Retorna una coleccion de clientes ordenados por cantidad de contactos
        /// </summary>
        /// <returns>
        ///     Una coleccion de clientes ordenados por cantidad de contactos
        /// </returns>
        IEnumerable<Cliente> ClientesPorCantidadDeContactos();

        /// <summary>
        ///     Retorna una coleccion con los productos que fueron facturados para un cliente dado en un anio dado
        /// </summary>
        /// <param name="anio">
        ///     El anio en el que se vendieron los productos
        /// </param>
        /// <param name="idCliente">
        ///     El cliente al que se vendieron los productos
        /// </param>
        /// <returns>
        ///     Una colleccion con los productos que fueron facturados para el cliente dado en el anio dado
        /// </returns>
        IEnumerable<Producto> InfoProductosClienteAnio(int anio, int idCliente);

        /// <summary>
        ///     Retorna una coleccion con los productos que fueron facturados en un anio y moneda dados
        /// </summary>
        /// <param name="anio">
        ///     El anio en el que se vendieron los productos
        /// </param>
        /// <param name="moneda">
        ///     La moneda en la que se vendieron los productos
        /// </param>
        /// <returns>
        ///     Una coleccion con los productos que fueron facturados en el anio y la moneda dados
        /// </returns>
        IEnumerable<Producto> InfoProductosMonedaAnio(int anio, Moneda moneda);
    }
}