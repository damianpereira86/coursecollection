﻿namespace Reports
{
    public class Cliente
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Id { get; set; }
        public string[] Telefonos { get; set; }
    }
}
