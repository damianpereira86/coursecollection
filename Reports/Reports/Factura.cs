﻿using System.Collections.Generic;

namespace Reports
{
    using Utils.Enums;

    public class Factura
    {
        public List<Linea> Lineas { get; set; }
        public Cliente Cliente { get; set; }
        public Moneda Moneda { get; set; }
        public int Anio { get; set; }
    }
}
