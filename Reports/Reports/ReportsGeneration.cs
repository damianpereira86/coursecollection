﻿using System;

namespace Reports
{
    using System.Linq;
    using Utils.Enums;
    using System.Collections.Generic;
    using Interfaces;

    public class ReportsGeneration : IReportsGeneration
    {
        ///<inheritdoc />
        public IEnumerable<Factura> FacturasPorAnio()
        {
            return Repositorio.Facturas.OrderBy(factura => factura.Anio);
        }

        ///<inheritdoc />
        public IEnumerable<Cliente> ClientesConFacturasPorAnio(int anio)
        {
            return (from factura in Repositorio.Facturas where factura.Anio.Equals(anio) select factura.Cliente).ToList();
        }

        ///<inheritdoc />
        public IEnumerable<IGrouping<Moneda, double>> TotalesFacturados()
        {
            var a = (from factura in Repositorio.Facturas
                     group factura by new {factura.Moneda, factura.Anio}
                     into grp
                            select new
                            {
                                grp.Key.Moneda,
                                grp.Key.Anio,
                                Suma = grp.Sum(t => t.Lineas.Sum(x => x.Valor))
                            });
            
            return null;
        }

        ///<inheritdoc />
        public IEnumerable<Cliente> ClientesPorCantidadDeContactos()
        {
            return (from cliente in Repositorio.Clientes orderby cliente.Telefonos.Count() select cliente).ToList();
        }

        ///<inheritdoc />
        public IEnumerable<Producto> InfoProductosClienteAnio(int anio, int idCliente)
        {
            return (from factura in Repositorio.Facturas where factura.Cliente.Id.Equals(idCliente) && (factura.Anio.Equals(anio)) from linea in factura.Lineas from producto in Repositorio.Productos where producto.Id.Equals(linea.IdProducto) select producto).ToList();
        }

        ///<inheritdoc />
        public IEnumerable<Producto> InfoProductosMonedaAnio(int anio, Moneda moneda)
        {
            var productos = new List<Producto>();

            foreach (var producto in from factura in Repositorio.Facturas where factura.Anio.Equals(anio) && factura.Moneda.Equals(moneda) from linea in factura.Lineas from producto in Repositorio.Productos where producto.Id.Equals(linea.IdProducto) where !productos.Contains(producto) select producto)
            {
                productos.Add(producto);
            }

            return productos;
        }
    }
}