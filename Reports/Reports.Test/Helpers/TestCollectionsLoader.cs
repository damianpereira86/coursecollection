﻿using System.CodeDom;
using System.Linq;

namespace Reports.Test.Helpers
{
    using System.Collections.Generic;
    using Utils.Enums;

    public class TestCollectionsLoader
    {
        public static void LoadProductos()
        {
            var productos = new List<Producto>();

            var producto1 = new Producto()
            {
                Id = 1,
                Nombre = "FIFA 17",
                ValorActual = 60,
                Descripcion = "Fifix"
            };
            productos.Add(producto1);

            var producto2 = new Producto()
            {
                Id = 2,
                Nombre = "MKX",
                ValorActual = 40,
                Descripcion = "Mortal Kombat 10"
            };
            productos.Add(producto2);

            Repositorio.Productos = productos;
        }

        public static void LoadClientes()
        {
            var clientes = new List<Cliente>();

            var telefonos1 = new[] { "099111111", "099222222", "099333333" };
            var cliente1 = new Cliente()
            {
                Id = 1,
                Nombre = "Ruben",
                Telefonos = telefonos1
            };
            clientes.Add(cliente1);

            var telefonos2 = new[] { "099111111" };
            var cliente2 = new Cliente()
            {
                Id = 2,
                Nombre = "Carlos",
                Telefonos = telefonos2
            };
            clientes.Add(cliente2);

            var telefonos3 = new[] { "099111111", "099222222"};
            var cliente3 = new Cliente()
            {
                Id = 3,
                Nombre = "Jose",
                Telefonos = telefonos3
            };
            clientes.Add(cliente3);

            Repositorio.Clientes = clientes;
        }

        public static void LoadFacturas()
        {
            var facturas = new List<Factura>();

            var linea1 = new Linea()
            {
                IdProducto = 1,
                Valor = 60
            };
            var linea2 = new Linea()
            {
                IdProducto = 2,
                Valor = 45
            };
            var factura1 = new Factura
            {
                Anio = 2015,
                Moneda = Moneda.Dolar,
                Cliente = Repositorio.Clientes.First(cliente => cliente.Nombre.Equals("Jose")),
                Lineas = new List<Linea> {linea1, linea2}
            };
            facturas.Add(factura1);

            var linea3 = new Linea()
            {
                IdProducto = 1,
                Valor = 65
            };
            var factura2 = new Factura
            {
                Anio = 2014,
                Moneda = Moneda.Euro,
                Cliente = Repositorio.Clientes.First(cliente => cliente.Nombre.Equals("Ruben")),
                Lineas = new List<Linea> {linea3}
            };
            facturas.Add(factura2);

            var linea4 = new Linea()
            {
                IdProducto = 2,
                Valor = 40
            };
            var factura3 = new Factura
            {
                Anio = 2016,
                Moneda = Moneda.Dolar,
                Cliente = Repositorio.Clientes.First(cliente => cliente.Nombre.Equals("Jose")),
                Lineas = new List<Linea> {linea4}
            };
            facturas.Add(factura3);

            var linea5 = new Linea()
            {
                IdProducto = 2,
                Valor = 38
            };
            var factura4 = new Factura
            {
                Anio = 2015,
                Moneda = Moneda.Dolar,
                Cliente = Repositorio.Clientes.First(cliente => cliente.Nombre.Equals("Ruben")),
                Lineas = new List<Linea> {linea5}
            };
            facturas.Add(factura4);

            Repositorio.Facturas = facturas;
        }
    }
}