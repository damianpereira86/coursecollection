﻿using System.Linq;
using Reports.Test.Helpers;
using Reports.Utils.Enums;

namespace Reports.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ReportsTests
    {
        [TestInitialize]
        public void MyTestInitialize()
        {
            TestCollectionsLoader.LoadProductos();
            TestCollectionsLoader.LoadClientes();
            TestCollectionsLoader.LoadFacturas();
        }

        [TestMethod]
        public void TestFacturasPorAnio()
        {
            var reports = new ReportsGeneration();
            var actual = reports.FacturasPorAnio().ToList();
            Assert.AreEqual(4, actual.Count);
            Assert.AreEqual(2014, actual[0].Anio);
            Assert.AreEqual(2015, actual[1].Anio);
            Assert.AreEqual(2015, actual[2].Anio);
            Assert.AreEqual(2016, actual[3].Anio);
        }

        [TestMethod]
        public void TestClientesConFacturasEn2015()
        {
            var reports = new ReportsGeneration();
            var actual = reports.ClientesConFacturasPorAnio(2015).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual("Jose", actual[0].Nombre);
            Assert.AreEqual("Ruben", actual[1].Nombre);
        }

        [TestMethod]
        public void TestClientesConFacturasEn2016()
        {
            var reports = new ReportsGeneration();
            var actual = reports.ClientesConFacturasPorAnio(2016).ToList();
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual("Jose", actual[0].Nombre);
        }

        [TestMethod]
        public void TestClientesPorCantiadadDeContactos()
        {
            var reports = new ReportsGeneration();
            var actual = reports.ClientesPorCantidadDeContactos().ToList();
            Assert.AreEqual(3, actual.Count);
            Assert.AreEqual("Carlos", actual[0].Nombre);
            Assert.AreEqual("Jose", actual[1].Nombre);
            Assert.AreEqual("Ruben", actual[2].Nombre);
        }

        [TestMethod]
        public void TestInfoProductosJose2015()
        {
            var reports = new ReportsGeneration();
            var actual = reports.InfoProductosClienteAnio(2015, 3).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual("FIFA 17", actual[0].Nombre);
            Assert.AreEqual("MKX", actual[1].Nombre);
        }

        [TestMethod]
        public void TestInfoProductosRuben2014()
        {
            var reports = new ReportsGeneration();
            var actual = reports.InfoProductosClienteAnio(2014, 1).ToList();
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual("FIFA 17", actual[0].Nombre);
        }

        [TestMethod]
        public void TestInfoProductosDolar2015()
        {
            var reports = new ReportsGeneration();
            var actual = reports.InfoProductosMonedaAnio(2015, Moneda.Dolar).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual("FIFA 17", actual[0].Nombre);
            Assert.AreEqual("MKX", actual[1].Nombre);
        }

        [TestMethod]
        public void TestInfoProductosEuro2014()
        {
            var reports = new ReportsGeneration();
            var actual = reports.InfoProductosMonedaAnio(2014, Moneda.Euro).ToList();
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual("FIFA 17", actual[0].Nombre);
        }
    }
}
