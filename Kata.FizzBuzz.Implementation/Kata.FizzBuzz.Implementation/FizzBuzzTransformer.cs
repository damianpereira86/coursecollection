﻿namespace Kata.FizzBuzz.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    public class FizzBuzzTransformer : IFizzBuzzTransformer
    {
        public IEnumerable<string> Transform(IEnumerable<int> sequence)
        {
            List<string> result = new List<string>();

            bool isInSequence = sequence.SequenceEqual(Enumerable.Range(1, sequence.Count()));

            if (isInSequence)
            {
                foreach (var i in sequence)
                {
                    if (i % 3 == 0 && i % 5 == 0)
                    {
                        result.Add("FizzBuzz");
                    }
                    else if (i % 3 == 0)
                    {
                        result.Add("Fizz");
                    }
                    else if (i % 5 == 0)
                    {
                        result.Add("Buzz");
                    }
                    else
                    {
                        result.Add(i.ToString());
                    }
                }
                return result;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}
